package com.example.request.response.loggingfilter.validator;

import com.example.request.response.loggingfilter.model.Customer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerValidatorTest {

    @Test
    @DisplayName("It should validate customer name")
    void itShouldValidateCustomerName() {

        CustomerValidator customerValidator = new CustomerValidator();
        customerValidator.registerRule(new CustomerNameNotEmpty());

        ValidationResult result = customerValidator.validate(new Customer(1L, null, "address"));

        assertAll(
                () ->assertEquals(1, result.errors().size()),
                () ->assertEquals("Customer name is empty", result.errors().get(0))
        );
    }

    @Test
    @DisplayName("It should validate customer address")
    void itShouldValidateCustomerAddress() {

        CustomerValidator customerValidator = new CustomerValidator();
        customerValidator.registerRule(new CustomerAddressNotEmpty());

        ValidationResult result = customerValidator.validate(new Customer(1L, "name", null));

        assertAll(
                () ->assertEquals(1, result.errors().size()),
                () ->assertEquals("Customer address is empty", result.errors().get(0))
        );
    }

    @Test
    @DisplayName("It should validate customer")
    void itShouldValidateCustomer() {

        CustomerValidator customerValidator = new CustomerValidator();
        customerValidator.registerRule(new CustomerAddressNotEmpty());
        customerValidator.registerRule(new CustomerNameNotEmpty());

        ValidationResult result = customerValidator.validate(new Customer(1L, null, null));

        assertAll(
                () ->assertEquals(2, result.errors().size()),
                () ->assertEquals("Customer address is empty", result.errors().get(0)),
                () ->assertEquals("Customer name is empty", result.errors().get(1))
        );
    }

}