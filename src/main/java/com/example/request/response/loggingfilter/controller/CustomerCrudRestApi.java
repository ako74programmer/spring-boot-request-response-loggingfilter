package com.example.request.response.loggingfilter.controller;

import com.example.request.response.loggingfilter.model.Customer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

@Tag(name = "customer", description = "the customer API")
public interface CustomerCrudRestApi {
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Find customer by ID", description = "Returns a single customer", tags = { "customer" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = @Content(schema = @Schema(implementation = Customer.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content) })
    public Customer findById(
            @Parameter(description = "ID of customer", required = true)
            @PathVariable("id") final long id,
            @NotNull @Parameter(description = "Select which kind of data to fetch", required = true)
            @Valid @RequestHeader(value="authorization", required = false) String authorization)
            throws Exception;

    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Get customers", description = "Returns a customer collection", tags = { "customer" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = { @Content(array = @ArraySchema(schema = @Schema(implementation = Customer.class))) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
    public Collection<Customer> findAll();

    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create customer", description = "Create customer", tags = { "customer" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class)),
                    @Content(mediaType = "application/xml", schema = @Schema(implementation = Customer.class))
            }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
    public Long create(
            @NotNull
            @Parameter(description = "Created object", required = true)
            @Valid @RequestBody Customer body,
            @NotNull @Parameter(description = "select which kind of data to fetch", required = true)
            @Valid @RequestHeader(value="authorization", required = false) String authorization)
            throws Exception;

    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Update customer", description = "Update customer", tags = { "customer" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class)),
                    @Content(mediaType = "application/xml", schema = @Schema(implementation = Customer.class))
            }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
    public void update(@PathVariable("id") final long id, @RequestBody final Customer body);

    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Create customer", description = "Create customer", tags = { "customer" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class)),
                    @Content(mediaType = "application/xml", schema = @Schema(implementation = Customer.class))
            }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content),
            @ApiResponse(responseCode = "403", description = "Forbidden", content = @Content)
    })
    public void delete(@PathVariable final long id);

}
