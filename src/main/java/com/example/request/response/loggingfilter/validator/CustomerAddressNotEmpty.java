package com.example.request.response.loggingfilter.validator;

import com.example.request.response.loggingfilter.model.Customer;
import org.apache.commons.lang3.StringUtils;

public class CustomerAddressNotEmpty implements ValidationRule {
    @Override
    public ValidationResult check(Customer customer) {
        var result = new ValidationResult();
        if (customer.address() == null ||  customer.address().equals(StringUtils.EMPTY)) {
            result.errors().add("Customer address is empty");
        }

        return result;
    }
}
