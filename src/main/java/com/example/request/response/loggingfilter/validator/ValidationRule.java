package com.example.request.response.loggingfilter.validator;

import com.example.request.response.loggingfilter.model.Customer;

public interface ValidationRule {
    ValidationResult check(Customer customer);
}
