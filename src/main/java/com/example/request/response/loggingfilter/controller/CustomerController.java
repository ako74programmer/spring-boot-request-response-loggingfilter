package com.example.request.response.loggingfilter.controller;

import com.example.request.response.loggingfilter.service.CustomerService;
import com.example.request.response.loggingfilter.model.Customer;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController implements CustomerCrudRestApi {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/")
    @Override
    public Collection<Customer> findAll() {
        return customerService.getCustomers();
    }

    @GetMapping("/{id}")
    @Override
    public Customer findById(@PathVariable long id, String authorization) throws Exception {
        return customerService.findCustomerById(id);
    }

    @PostMapping("/")
    @Override
    public Long create(Customer body, String authorization) throws Exception {
        return customerService.saveCustomer(body);
    }

    @PutMapping(value = "/{id}")
    @Override
    public void update(long id, Customer body) {
        customerService.updateCustomer(id, body);
    }

    @DeleteMapping("/{id}")
    @Override
    public void delete(@PathVariable("id") long id) {
        customerService.deleteCustomer(id);
    }
}
