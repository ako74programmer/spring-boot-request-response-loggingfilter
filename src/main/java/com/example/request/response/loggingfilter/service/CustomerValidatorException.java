package com.example.request.response.loggingfilter.service;

public class CustomerValidatorException extends RuntimeException {
    public CustomerValidatorException(String message) {
        super(message);
    }
}
