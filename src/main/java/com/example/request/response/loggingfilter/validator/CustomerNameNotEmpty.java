package com.example.request.response.loggingfilter.validator;

import com.example.request.response.loggingfilter.model.Customer;
import org.apache.commons.lang3.StringUtils;

public class CustomerNameNotEmpty implements ValidationRule {
    @Override
    public ValidationResult check(Customer customer) {
        var result = new ValidationResult();
        if (customer.name() == null ||  customer.name().equals(StringUtils.EMPTY)) {
            result.errors().add("Customer name is empty");
        }

        return result;
    }
}
