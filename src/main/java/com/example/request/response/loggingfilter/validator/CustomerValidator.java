package com.example.request.response.loggingfilter.validator;

import com.example.request.response.loggingfilter.model.Customer;

import java.util.ArrayList;
import java.util.List;

public class CustomerValidator {
    private final List<ValidationRule> rules = new ArrayList<>();

    public void registerRule(ValidationRule rule) {
        this.rules.add(rule);
    }

    public ValidationResult validate(Customer customer) {
        var combinedValidationResult = new ValidationResult();
        rules.forEach(rule -> {
            var result = rule.check(customer);
            combinedValidationResult.errors().addAll(result.errors());
        });

        return combinedValidationResult;
    }

}

