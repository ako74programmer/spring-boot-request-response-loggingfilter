package com.example.request.response.loggingfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequestResponseLoggingFilterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RequestResponseLoggingFilterApplication.class, args);
    }

}
