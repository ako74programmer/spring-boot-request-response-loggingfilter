package com.example.request.response.loggingfilter.service;

import com.example.request.response.loggingfilter.model.Customer;
import com.example.request.response.loggingfilter.validator.CustomerAddressNotEmpty;
import com.example.request.response.loggingfilter.validator.CustomerNameNotEmpty;
import com.example.request.response.loggingfilter.validator.CustomerValidator;
import com.example.request.response.loggingfilter.validator.ValidationResult;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {
    private Long counter = 4L;
    private List<Customer> customers = new ArrayList<>(
            List.of(
                    new Customer(1L, "customer1", "address1"),
                    new Customer(2L, "customer2", "address2"),
			        new Customer(3L, "customer3", "address3"))
             );

    public List<Customer> getCustomers() {
        return customers;
    }

    public Long saveCustomer(Customer customer) {
        validateCustomer(customer);
        Customer newCustomer = new Customer(counter++, customer.name(), customer.address());
        customers.add(newCustomer);
        return newCustomer.id();
    }

    public Customer findCustomerById(long id) {
        return customers.stream().filter(c -> c.id() == id).findFirst().orElseThrow();
    }


    public void updateCustomer(long id, Customer customer) {
        Customer delCustomer = customers.stream().filter(c -> c.id() == id).findFirst().orElseThrow();
        customers.remove(delCustomer);
        Customer newCustomer = new Customer(id, customer.name(), customer.address());
        customers.add(newCustomer);
    }

    public void deleteCustomer(long id) {
        Customer delCustomer = customers.stream().filter(c -> c.id() == id).findFirst().orElseThrow();
        customers.remove(delCustomer);
    }

    private void validateCustomer(Customer customer) {
        CustomerValidator customerValidator = new CustomerValidator();
        customerValidator.registerRule(new CustomerAddressNotEmpty());
        customerValidator.registerRule(new CustomerNameNotEmpty());

        ValidationResult result = customerValidator.validate(customer);

        if (!result.errors().isEmpty()) {
            throw new CustomerValidatorException(result.toString());
        }

    }
}
