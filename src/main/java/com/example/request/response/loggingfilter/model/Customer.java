package com.example.request.response.loggingfilter.model;

public record Customer(Long id, String name, String address) {}
